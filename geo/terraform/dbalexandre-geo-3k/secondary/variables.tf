variable "prefix" {
  default = "geo-3k-dbalexandre-east4"
}

variable "project" {
  default = "group-geo-f9c951"
}

variable "region" {
  default = "us-east4"
}

variable "zone" {
  default = "us-east4-c"
}

variable "geo_site" {
  default = "us-east4"
}

variable "geo_deployment" {
  default = "3k-3k-dbalexandre"
}

variable "machine_image" {
  default = "ubuntu-2004-lts"
}
