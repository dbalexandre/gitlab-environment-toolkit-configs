terraform {
  backend "gcs" {
    bucket = "dbalexandre-geo-3k-terraform-state"
    prefix = "geo-3k-dbalexandre-secondary"
  }
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 3"
    }
  }
}

provider "google" {
  project = var.project
  region  = var.region
  zone    = var.zone
}

resource "google_compute_address" "external_ip" {
  name   = "${var.prefix}-external"
  region = var.region
}

output "external_ip_addr" {
  value = google_compute_address.external_ip.address
}
