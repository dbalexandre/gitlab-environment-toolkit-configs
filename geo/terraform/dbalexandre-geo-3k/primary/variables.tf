variable "prefix" {
  default = "geo-3k-dbalexandre-east1"
}

variable "project" {
  default = "group-geo-f9c951"
}

variable "region" {
  default = "us-east1"
}

variable "zone" {
  default = "us-east1-c"
}

variable "geo_site" {
  default = "us-east1"
}

variable "geo_deployment" {
  default = "3k-3k-dbalexandre"
}

variable "machine_image" {
  default = "ubuntu-2004-lts"
}
